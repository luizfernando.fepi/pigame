package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import br.com.fepi.PIGameV2.R;

public class EntrarAluno extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrar_aluno);
    }

    public void entrar(View view){
        Intent intent = new Intent(this, Tabuleiro.class);
        startActivity(intent);
    }
}
