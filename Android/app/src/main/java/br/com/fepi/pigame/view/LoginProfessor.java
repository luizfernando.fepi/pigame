package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import br.com.fepi.PIGameV2.R;
import br.com.fepi.pigame.MySingleton;
import br.com.fepi.pigame.controller.ControleProfessor;
import br.com.fepi.pigame.controller.DadosAplicacao;
import br.com.fepi.pigame.controller.DadosProfLogado;

import static android.widget.Toast.LENGTH_LONG;

public class LoginProfessor extends AppCompatActivity {

    private EditText senha_campo;
    private EditText email_campo;
    private Context LoginContext;

    private String email;
    private String senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_professor);
        senha_campo = findViewById(R.id.senhaProfesorText);
        email_campo = findViewById(R.id.emailProfessorText);
        LoginContext=this;
    }

    public void login(View view) {

        senha = senha_campo.getText().toString();
        email = email_campo.getText().toString();
        String ServerURL = DadosAplicacao.getInstance().getURL() + "get_prof.php?senha=\"" + senha + "\"&email=\"" + email+"\"";

        new ControleProfessor(ServerURL).execute();

        if(DadosAplicacao.getInstance().getProfessorLogado() != null){
            Toast.makeText(LoginContext.getApplicationContext(),"LOGIN REALIZADO!", LENGTH_LONG).show();
            Intent intent = new Intent(LoginContext, MenuProfessor.class);
            startActivity(intent);
        }else{
            Toast.makeText(LoginContext.getApplicationContext(),"Erro ao tentar logar", LENGTH_LONG).show();
        }

    }

    /*
    public void login(View view) {

        senha= senha_campo.getText().toString();
        email= email_campo.getText().toString();
        String ServerURL = DadosAplicacao.getInstance().getURL()+"get_prof.php?senha="+senha+"&email="+email;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, ServerURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            DadosProfLogado.DadosLogin(response.getString("nome"),response.getString("email"));
                            if(email.equals("") && senha.equals("")){
                                Toast.makeText(LoginContext.getApplicationContext(),"Por favor digite um email e uma senha", LENGTH_LONG).show();
                            }
                            else if(email.equals("")){
                                Toast.makeText(LoginContext.getApplicationContext(),"Por favor digite um email", LENGTH_LONG).show();
                            }
                            else if(senha.equals("")){
                                Toast.makeText(LoginContext.getApplicationContext(),"Por favor digite a senha", LENGTH_LONG).show();
                            }
                            else if(email.equals(DadosProfLogado.getEmail()))// se tudo estiver certo ira para a o menu do professor
                            {
                                Intent intent = new Intent(LoginContext, MenuProfessor.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(LoginContext.getApplicationContext(),"Email ou senha invalidos", LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(LoginContext.getApplicationContext(),"Erro ao tentar logar", LENGTH_LONG).show();
                            System.out.println("Mensagem erro: "+ e.toString());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        System.out.println("Mensagem erro: " + error.toString());

                    }

                }
                );
// Access the RequestQueue through your singleton class.
       MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }*/

    public void cadastro(View view){
        Intent intent = new Intent(this, CadastroProfessor.class);
        startActivity(intent);
    }
}
