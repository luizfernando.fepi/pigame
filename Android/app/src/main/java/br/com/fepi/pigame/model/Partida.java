package br.com.fepi.pigame.model;

import java.util.ArrayList;

public class Partida {

    private String codigoAcesso;
    private Tabuleiro tabueleiro;
    private ArrayList<Equipe> equipes = new ArrayList<>();
    private ArrayList<Tema> temas = new ArrayList<>();
    private Professor professor;

    public Partida(String codigoAcesso) {
        this.codigoAcesso = codigoAcesso;
    }

    public void addTema(Tema t){
        temas.add(t);
    }

    public void removeTema(String nomeTema){
        for (Tema t:temas) {
            if(t.getNome().equals(nomeTema)){
                temas.remove(t);
            }
        }
    }

    public String getCodigoAcesso() {
        return codigoAcesso;
    }

    public void setCodigoAcesso(String codigoAcesso) {
        this.codigoAcesso = codigoAcesso;
    }

    public Tabuleiro getTabueleiro() {
        return tabueleiro;
    }

    public void setTabueleiro(Tabuleiro tabueleiro) {
        this.tabueleiro = tabueleiro;
    }

    public ArrayList<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(ArrayList<Equipe> equipes) {
        this.equipes = equipes;
    }

    public ArrayList<Tema> getTemas() {
        return temas;
    }

    public void setTemas(ArrayList<Tema> temas) {
        this.temas = temas;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
}
