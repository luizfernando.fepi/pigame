package br.com.fepi.pigame.controller;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;


public class cadastrar_prof extends AsyncTask<String,Integer,String> {

    String nome;
    String senha;
    String email;
    Context CadastroContext;
    public cadastrar_prof(String nome, String senha, String email, Context CadastroContext)

    {
        this.nome=nome;
        this.senha=senha;
        this.email=email;
        this.CadastroContext = CadastroContext;
    }

    @Override
    protected String doInBackground(String... params) {

        try{
            String nome = this.nome;
            String senha = this.senha;
            String email = this.email;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair("nome", nome));
            nameValuePairs.add(new BasicNameValuePair("senha", senha));
            nameValuePairs.add(new BasicNameValuePair("email", email));

            String ServerURL = DadosAplicacao.getInstance().getURL()+"registra_prof.php";
            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(ServerURL);

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse httpResponse = httpClient.execute(httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();

            System.out.println("Mensagem: ");

            return new String ("Funciona");
        }
        catch(Exception e){

            System.out.println("Mensagem erro: "+e.getMessage());
            return new String("Erro");
        }
    }

    @Override
    protected void onPostExecute(String text){//text recebe o retorno do metodo DoInBackGround
        if(text.equals("Funciona"))
        Toast.makeText(CadastroContext,"Cadastrado com sucesso", Toast.LENGTH_LONG).show();
        else if(text.equals("Erro"))
            Toast.makeText(CadastroContext,"Falha no cadastro", Toast.LENGTH_LONG).show();
    }
}




//String link = "localhost/view/registra_prof.ajax.php";
           /* String data  = URLEncoder.encode("nome", "UTF-8") + "=" +
                    URLEncoder.encode(nome, "UTF-8");
            data += "&" + URLEncoder.encode("senha", "UTF-8") + "=" +
                    URLEncoder.encode(senha, "UTF-8");
            data += "&" + URLEncoder.encode("email", "UTF-8") + "=" +
                    URLEncoder.encode(email, "UTF-8");
            data += "&" + URLEncoder.encode("0", "UTF-8") + "=" +
                    URLEncoder.encode(senha, "UTF-8");*/
/*
            URL url = new URL(link);
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI(link));
            HttpResponse response = client.execute(request);
            BufferedReader in = new BufferedReader(new
                    InputStreamReader(response.getEntity().getContent()));
*/
          /*  StringBuffer sb = new StringBuffer("");
            String line="";

            while ((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }*/
          //in.close();