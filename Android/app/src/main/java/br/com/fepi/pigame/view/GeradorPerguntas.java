package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import br.com.fepi.PIGameV2.R;
import br.com.fepi.pigame.MySingleton;
import br.com.fepi.pigame.controller.DadosAplicacao;

import static android.widget.Toast.LENGTH_LONG;

public class GeradorPerguntas extends AppCompatActivity {

    private boolean modificarExistente;

    Context perguntaContext;

    private Switch perguntaSwitch ;

    private Spinner spinnerPergunta;

    private EditText perguntaText;
    private EditText resp1Text;
    private EditText resp2Text;
    private EditText resp3Text;
    private EditText resp4Text;

    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    RadioButton radioButton4;

    String respCorreta;
    int temaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gerador_perguntas);

        perguntaContext=this;

        perguntaSwitch = findViewById(R.id.novaPerguntaSwitch);

        spinnerPergunta = findViewById(R.id.spinnerPergunta);
        spinnerPergunta.setVisibility(View.INVISIBLE);

        perguntaText = findViewById(R.id.perguntaText);
        resp1Text = findViewById(R.id.resp1Text);
        resp2Text = findViewById(R.id.resp2Text);
        resp3Text = findViewById(R.id.resp3Text);
        resp4Text = findViewById(R.id.resp4Text);

        modificarExistente = false;

        radioButton1 = findViewById(R.id.radioButton);
        radioButton2 = findViewById(R.id.radioButton2);
        radioButton3 = findViewById(R.id.radioButton3);
        radioButton4 = findViewById(R.id.radioButton4);

        respCorreta="notChecked";

        Bundle extras = getIntent().getExtras();
        temaId=extras.getInt("idTema");
    }

    public void radioButtonMarcado(View view){
        if(radioButton1.isChecked()){
            respCorreta = "a";
        }
        if(radioButton2.isChecked()){
            respCorreta = "b";
        }
        if(radioButton3.isChecked()){
            respCorreta = "c";
        }
        if(radioButton4.isChecked()){
            respCorreta = "d";
        }
    }

    public void trocarmodExistente(View view){
        if(perguntaSwitch.isChecked())
        {
            spinnerPergunta.setVisibility(View.VISIBLE);
            perguntaText.setVisibility(View.INVISIBLE);
            modificarExistente=true;
        }else{
            modificarExistente=false;
            spinnerPergunta.setVisibility(View.INVISIBLE);
            perguntaText.setVisibility(View.VISIBLE);
        }
    }

    public void enviar(View view){
        //idTema
        String pergunta = perguntaText.getText().toString();
        String resp1 = resp1Text.getText().toString();
        String resp2 = resp2Text.getText().toString();
        String resp3 = resp3Text.getText().toString();
        String resp4 = resp4Text.getText().toString();
        //respCorreta

        String ServerURL = DadosAplicacao.getInstance().getURL()+"registra_pergunta.php?id=0&idtema="+temaId+"&pergunta="+pergunta+
                "&resp1="+resp1+"&resp2="+resp2+"&resp3="+resp3+"&resp4="+resp4+"&respCorreta="+respCorreta;

        if((resp1.equals("") || resp2.equals("") || resp3.equals("") || resp4.equals("")) && pergunta.equals("")) {
            Toast.makeText(perguntaContext.getApplicationContext(), "Por favor preencha a pergunta e todas as respostas", LENGTH_LONG).show();
            return;
        }
        else if(resp1.equals("") || resp2.equals("") || resp3.equals("") || resp4.equals("")) {
            Toast.makeText(perguntaContext.getApplicationContext(), "Por favor preencha todas as respostas", LENGTH_LONG).show();
            return;
        }
        else if(pergunta.equals("")) {
            Toast.makeText(perguntaContext.getApplicationContext(), "Por favor preencha a pergunta", LENGTH_LONG).show();
            return;
        }
        else if(respCorreta.equals("notChecked"))
        {
            Toast.makeText(perguntaContext.getApplicationContext(), "Marque a resposta que sera a correta", LENGTH_LONG).show();
            return;
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, ServerURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if(response.getString("sucesso").equals("1")) {
                                Toast.makeText(perguntaContext.getApplicationContext(), "Pergunta inserida com sucesso", LENGTH_LONG).show();

                            }
                            else if(response.getString("sucesso").equals("0"))
                                Toast.makeText(perguntaContext.getApplicationContext(),"Pergunta nao pode ser inserido, possivelmente ja existe", LENGTH_LONG).show();

                        } catch (Exception e) {
                            Toast.makeText(perguntaContext.getApplicationContext(),"Erro"+e.getMessage(), LENGTH_LONG).show();
                            System.out.println("Mensagem erro: "+ e.toString());
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(perguntaContext.getApplicationContext(),"Erro"+error.getMessage(), LENGTH_LONG).show();
                        System.out.println("Mensagem erro: " + error.toString());

                    }

                }
                );

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);


    }


}
