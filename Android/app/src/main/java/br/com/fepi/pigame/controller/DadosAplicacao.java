package br.com.fepi.pigame.controller;

import br.com.fepi.pigame.model.Equipe;
import br.com.fepi.pigame.model.Partida;
import br.com.fepi.pigame.model.Professor;

/***
 * Classe do tipo Singleton para armazenar dados que serão acessados pelas activities
 */
public class DadosAplicacao {

   private static DadosAplicacao dadosInstance;
   private String URL="pigametcc.azurewebsites.net/view/";  //URL do webservice

   private Professor professorLogado;
   private Equipe equipeLogada;

   private Partida partida;

   private DadosAplicacao(){

   }

   public static DadosAplicacao getInstance(){
      if(dadosInstance == null){
         return new DadosAplicacao();
      }
      return dadosInstance;
   }

   public String getURL() {
      return URL;
   }

   public void setURL(String URL) {
      this.URL = URL;
   }

   public Professor getProfessorLogado() {
      return professorLogado;
   }

   public void setProfessorLogado(Professor professorLogado) {
      this.professorLogado = professorLogado;
   }

   public Equipe getEquipeLogada() {
      return equipeLogada;
   }

   public void setEquipeLogada(Equipe equipeLogada) {
      this.equipeLogada = equipeLogada;
   }

   public Partida getPartida() {
      return partida;
   }

   public void setPartida(Partida partida) {
      this.partida = partida;
   }
}
