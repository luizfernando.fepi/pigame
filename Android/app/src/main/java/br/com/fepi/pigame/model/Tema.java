package br.com.fepi.pigame.model;

import java.util.ArrayList;

public class Tema {
    private String nome;
    private ArrayList<Disciplina> disciplinas = new ArrayList<>();

    public Tema(String nome, ArrayList<Disciplina> disciplinas) {
        this.nome = nome;
        this.disciplinas = disciplinas;
    }

    public void addDisciplina(Disciplina d){
        disciplinas.add(d);
    }

    public void removeDisciplina(String nomeDisc){
        for (Disciplina d:disciplinas) {
            if(d.getNome().equals(nomeDisc)){
                disciplinas.remove(d);
            }
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(ArrayList<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }
}
