package br.com.fepi.pigame.model;

import java.util.ArrayList;
import java.util.Random;

public class Tabuleiro {

    private int qtdCasas;
    private ArrayList<TipoCasa> casas = new ArrayList<>();

    /**
     * @param qtdCasas é a quantidade de casas do tabuleiro
     * @param qtdD é quantidade de casas do tipo Desafio
     * @param qtdP é a quantidade de casas do tipo Premio_Punicao
     * */
    public Tabuleiro(int qtdCasas, int qtdD, int qtdP) {
        this.qtdCasas = qtdCasas;

        //inicializa todas as casas com o tipo comum
        for(int i=0; i<qtdCasas;i++){
            casas.add(TipoCasa.COMUM));
        }
        //define as casas do tipo desafio
        for(int i=0; i<qtdD;i++){
            int pos = -1;
            while(casas.get(pos)!=TipoCasa.COMUM) {
                pos = new Random().nextInt(qtdCasas);
            }
            casas.add(TipoCasa.DESAFIO);
        }

        //define as casas do tipo premio_punicao
        for(int i=0; i<qtdD;i++){
            int pos = -1;
            while(casas.get(pos)!=TipoCasa.COMUM) {
                pos = new Random().nextInt(qtdCasas);
            }
            casas.add(TipoCasa.PREMIO_PUNICAO);
        }

    }

    public int getQtdCasas() {
        return qtdCasas;
    }

    public void setQtdCasas(int qtdCasas) {
        this.qtdCasas = qtdCasas;
    }

    public ArrayList<TipoCasa> getCasas() {
        return casas;
    }

    public void setCasas(ArrayList<TipoCasa> casas) {
        this.casas = casas;
    }
}
