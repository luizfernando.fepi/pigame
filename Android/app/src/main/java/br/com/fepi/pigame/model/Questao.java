package br.com.fepi.pigame.model;

import java.util.ArrayList;

public class Questao {
    private int id;
    private String descricao;
    private ArrayList<String> alternativas = new ArrayList<>();
    private int respostaCorreta; //umas das posições de alternativas
    private ArrayList<Tema> temas = new ArrayList<>();

    public Questao(int id, String descricao, ArrayList<String> alternativas, int respostaCorreta, ArrayList<Tema> temas) {
        this.id = id;
        this.descricao = descricao;
        this.alternativas = alternativas;
        this.respostaCorreta = respostaCorreta;
        this.temas = temas;
    }

    public void addTema(Tema t){
        temas.add(t);
    }

    public void removeTema(String nomeDisc){
        for (Tema t:temas) {
            if(t.getNome().equals(nomeDisc)){
                temas.remove(t);
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ArrayList<String> getAlternativas() {
        return alternativas;
    }

    public void setAlternativas(ArrayList<String> alternativas) {
        this.alternativas = alternativas;
    }

    public int getRespostaCorreta() {
        return respostaCorreta;
    }

    public void setRespostaCorreta(int respostaCorreta) {
        this.respostaCorreta = respostaCorreta;
    }
}
