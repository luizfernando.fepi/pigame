package br.com.fepi.pigame.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JsonRequest {

    /***
     *
     * @param uri endpoint para consumo da API
     * @param metodo (GET, POST, PUT, DELETE, ...)
     * @return string no formato json
     * @throws Exception
     */
    public static String request( String uri, String metodo ) throws Exception {

        StringBuilder jsonString = new StringBuilder();
        try {
            System.out.println("Uri enviada: "+ uri);
            URL url = new URL(uri);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod(metodo);

        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        BufferedReader r = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = r.readLine()) != null) {
            jsonString.append(line);
        }

        urlConnection.disconnect();
        }catch (MalformedURLException e){
            return "MalformedURLException";
        }

        return jsonString.toString();
    }
}