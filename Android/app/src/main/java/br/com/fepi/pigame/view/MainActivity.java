package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import br.com.fepi.PIGameV2.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void entrarProfessor(View view){
        Intent intent = new Intent(this, LoginProfessor.class);
        startActivity(intent);
    }

    public void entrarAluno(View view)
    {
        Intent intent = new Intent(this, EntrarAluno.class);
        startActivity(intent);
    }
}
