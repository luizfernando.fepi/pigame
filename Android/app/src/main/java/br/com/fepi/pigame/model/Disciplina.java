package br.com.fepi.pigame.model;

import java.util.ArrayList;

public class Disciplina {
    private String nome;
    private ArrayList<Tema> temas = new ArrayList<>();

    public Disciplina(String nome, ArrayList<Tema> temas) {
        this.nome = nome;
        this.temas = temas;
    }

    public void addTema(Tema t){
        temas.add(t);
    }

    public void removeTema(String nomeTema){
        for (Tema t:temas) {
            if(t.getNome().equals(nomeTema)){
                temas.remove(t);
            }
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
