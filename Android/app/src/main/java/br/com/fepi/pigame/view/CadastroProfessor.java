package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import br.com.fepi.PIGameV2.R;
import br.com.fepi.pigame.controller.cadastrar_prof;

public class CadastroProfessor extends AppCompatActivity {//testar depois mudancas cadastros

    private EditText nome_campo;
    private EditText senha_campo;
    private EditText email_campo;
    private Context cadastroContext;

   // private String register_url = "192.168.0.112/pigame/view/registra_prof.ajax.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_professor);

        nome_campo = findViewById(R.id.nomeProfesorCadastro);
        senha_campo = findViewById(R.id.senhaProfesorCadastro);
        email_campo = findViewById(R.id.emailProfessorCadastro);
        cadastroContext=this;

    }

    public void cadastrar(View view){

        String nome= nome_campo.getText().toString();
        String senha= senha_campo.getText().toString();
        String email= email_campo.getText().toString();


            cadastrar_prof cadast = new cadastrar_prof(nome, senha, email, cadastroContext);
            cadast.execute();

            //Limpa os campos depois do cadastro

       //     Toast.makeText(this,"Cadastrado com sucesso",Toast.LENGTH_LONG).show();
            nome_campo.setText("");
            senha_campo.setText("");
            email_campo.setText("");

    }

 /*   public void cadastrarProf(){


        try {



        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    // JSONObject request = new JSONObject();


    //Populate the request parameters
    //   request.put(nome_campo, username);
    //  request.put(senha_campo, password);
    // request.put(email_campo, fullName);

/*
* String nome= nome_campo.getText().toString();
    String senha= senha_campo.getText().toString();
    String email= email_campo.getText().toString();

* */

}

