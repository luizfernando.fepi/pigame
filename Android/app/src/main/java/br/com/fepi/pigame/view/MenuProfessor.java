package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.com.fepi.PIGameV2.R;
import br.com.fepi.pigame.controller.DadosProfLogado;

public class MenuProfessor extends AppCompatActivity {

    private TextView olaProf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_professor);

        olaProf = findViewById(R.id.textViewOlaProf);
        olaProf.setText("Ola "+ DadosProfLogado.getNome());
    }

    public void criarPartida(View view){
        Intent intent = new Intent(this, CriarPartida.class);
        startActivity(intent);
    }

    public void irGerenciarTema(View view){
        Intent intent = new Intent(this, GerenciarTema.class);
        startActivity(intent);
    }
}

