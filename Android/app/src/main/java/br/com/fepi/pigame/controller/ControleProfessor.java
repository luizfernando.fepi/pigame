package br.com.fepi.pigame.controller;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.util.ArrayList;

import br.com.fepi.pigame.model.Professor;
import br.com.fepi.pigame.model.Tema;

public class ControleProfessor extends AsyncTask<Void, Void, Professor> {

    private String URL;

    public ControleProfessor(String URL) {
        this.URL = URL;
    }

    @Override
    protected Professor doInBackground(Void... voids) {

        try {
            String result = JsonRequest.request(URL,"GET");

            if(result.equals("MalformedURLException")){
                System.out.println("URL MAL FORMADA");
            }
            Gson gson = new Gson();
            return gson.fromJson( result, Professor.class );

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {

    }

    @Override
    protected void onPostExecute(Professor professor) {
        if(professor != null) {
            System.out.println("LOGIN REALIZADO: \n" + professor.toString());
            DadosAplicacao.getInstance().setProfessorLogado(professor);
        }else{
            System.out.println("FALHA NO LOGIN:");
        }
    }
}
