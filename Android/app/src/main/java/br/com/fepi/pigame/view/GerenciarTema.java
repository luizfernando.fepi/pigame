package br.com.fepi.pigame.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.fepi.PIGameV2.R;
import br.com.fepi.pigame.MySingleton;
import br.com.fepi.pigame.controller.DadosAplicacao;

import static android.widget.Toast.LENGTH_LONG;

public class GerenciarTema extends AppCompatActivity {
/// Falta editar e deletar

    Context gerenciTemaContext;

    private EditText tema_campo;
    private Spinner tema_spinner;

    private Button criar_button;
    private Button editar_button;
    private Button delete_button;

    private Switch editCreateSwitch;

    private String temaSelec;

    private Integer idSelec;
    private int op;

    ArrayList<String> listaTema;
    ArrayList<Integer> listaIdTema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gerenciar_tema);

        op=0;

        gerenciTemaContext = this;
        tema_campo = findViewById(R.id.TextBoxViewTema);
        tema_spinner = findViewById(R.id.spinnerTemas);

        listaTema = new ArrayList<>();
        listaIdTema = new ArrayList<>();

        criar_button = findViewById(R.id.buttonCriar);
        editar_button = findViewById(R.id.buttonEdit);
        delete_button = findViewById(R.id.buttonDeletar);

        editCreateSwitch = findViewById(R.id.switchEditCreate);

        loadSpinnerData();

        ///
        tema_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                temaSelec = tema_spinner.getItemAtPosition(tema_spinner.getSelectedItemPosition()).toString();

                int pos = (int) tema_spinner.getItemIdAtPosition(tema_spinner.getSelectedItemPosition());
                idSelec=listaIdTema.get(pos);///Pega o id do tema selecionado

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });///

    }

    public void loadSpinnerData() {///Carrega os dados para o spinner.
        // op==0 carrega todos, op==1 carrega apenas o ultimo
        String ServerURL = DadosAplicacao.getInstance().getURL()+"lista_tema.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, ServerURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                                JSONArray ArrayJSON = response.getJSONArray("dados");
                                if(op==0) {
                                    for (int i = 0; i < ArrayJSON.length(); i++) {
                                        JSONObject objetoJSON = ArrayJSON.getJSONObject(i);
                                        Integer id = Integer.parseInt(objetoJSON.getString("id"));
                                        String tema = objetoJSON.getString("tema");
                                        listaTema.add(tema);
                                        listaIdTema.add(id);
                                    }
                                }
                                else if(op==1)
                                {
                                    JSONObject objetoJSON = ArrayJSON.getJSONObject(ArrayJSON.length()-1);
                                    Integer id = Integer.parseInt(objetoJSON.getString("id"));
                                    String tema = objetoJSON.getString("tema");
                                    listaTema.add(tema);
                                    listaIdTema.add(id);
                                }
                                tema_spinner.setAdapter(new ArrayAdapter<String>(gerenciTemaContext, android.R.layout.simple_spinner_dropdown_item, listaTema));

                        } catch (Exception e) {
                            Toast.makeText(gerenciTemaContext.getApplicationContext(),"Erro"+e.getMessage(), LENGTH_LONG).show();
                            System.out.println("Mensagem erro: "+ e.toString());
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(gerenciTemaContext.getApplicationContext(),"Erro"+error.getMessage(), LENGTH_LONG).show();
                        System.out.println("Mensagem erro: " + error.toString());

                    }

                }
                );

        br.com.fepi.pigame.MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    public void trocar(View view){
        if(editCreateSwitch.isChecked()) {

            editar_button.setVisibility(View.VISIBLE);
            delete_button.setVisibility(View.VISIBLE);
            tema_spinner.setVisibility(View.VISIBLE);

            criar_button.setVisibility(View.INVISIBLE);
            tema_campo.setVisibility(View.INVISIBLE);
        } else  {
            editar_button.setVisibility(View.INVISIBLE);
            delete_button.setVisibility(View.INVISIBLE);
            tema_spinner.setVisibility(View.INVISIBLE);

            criar_button.setVisibility(View.VISIBLE);
            tema_campo.setVisibility(View.VISIBLE);
        }

    }

    public void editar(View view){
        Intent intent = new Intent(this, GeradorPerguntas.class);
        intent.putExtra("idTema",idSelec);
        startActivity(intent);

    }

    public void criar(View view){

        String tema = tema_campo.getText().toString();

        String ServerURL = DadosAplicacao.getInstance().getURL()+"registra_tema.php?tema="+tema+"&id="+0;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, ServerURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if(response.getString("sucesso").equals("1")) {
                                Toast.makeText(gerenciTemaContext.getApplicationContext(), "Tema inserido com sucesso", LENGTH_LONG).show();
                                op=1;
                                loadSpinnerData();
                            }
                            else if(response.getString("sucesso").equals("0"))
                                Toast.makeText(gerenciTemaContext.getApplicationContext(),"Tema nao pode ser inserido, possivelmente ja existe", LENGTH_LONG).show();

                        } catch (Exception e) {
                            Toast.makeText(gerenciTemaContext.getApplicationContext(),"Erro"+e.getMessage(), LENGTH_LONG).show();
                            System.out.println("Mensagem erro: "+ e.toString());
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Toast.makeText(gerenciTemaContext.getApplicationContext(),"Erro"+error.getMessage(), LENGTH_LONG).show();
                        System.out.println("Mensagem erro: " + error.toString());

                    }

                }
                );

// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }

    public void deletar(View view){

    }
}
