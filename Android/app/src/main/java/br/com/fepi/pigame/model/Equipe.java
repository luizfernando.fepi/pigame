package br.com.fepi.pigame.model;

public class Equipe {
    private String nome;
    private String codigoAcesso;
    private int posicaoTabuleiro = 0;
    private Tema tema;
    private int qtdDesafio = 0;
    private int qtdRotacao = 0;
    private String cor;

    public Equipe(String nome) {
        this.nome = nome;
    }

    public void addDesafio(){
        this.qtdDesafio++;
    }

    public void usarDesafio(){
        this.qtdDesafio--;
    }

    public void addRotacao(){
        this.qtdRotacao++;
    }

    public void usarRotacao(){
        this.qtdRotacao--;
    }

    public void avançar(){
        this.posicaoTabuleiro++;
    }

    public void retroceder(){
        this.posicaoTabuleiro--;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoAcesso() {
        return codigoAcesso;
    }

    public void setCodigoAcesso(String codigoAcesso) {
        this.codigoAcesso = codigoAcesso;
    }

    public int getPosicaoTabuleiro() {
        return posicaoTabuleiro;
    }

    public void setPosicaoTabuleiro(int posicaoTabuleiro) {
        this.posicaoTabuleiro = posicaoTabuleiro;
    }

    public Tema getTema() {
        return tema;
    }

    public void setTema(Tema tema) {
        this.tema = tema;
    }

    public int getQtdDesafio() {
        return qtdDesafio;
    }

    public void setQtdDesafio(int qtdDesafio) {
        this.qtdDesafio = qtdDesafio;
    }

    public int getQtdRotacao() {
        return qtdRotacao;
    }

    public void setQtdRotacao(int qtdRotacao) {
        this.qtdRotacao = qtdRotacao;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}
