<?php	
	function conectar()
	{
		$con = new PDO("mysql:host=MeuServidorMysql.mysql.database.azure.com; port=3306;
						dbname=pigame",
						"pigame@MeuServidorMysql",
						"pigame", 
						array(
    						PDO::MYSQL_ATTR_SSL_CA => '/ssh/BaltimoreCyberTrustRoot.crt.pem',
						PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false));
		if(!$con){
			return NULL;
		}
		$query = $con->prepare("SET NAMES utf8");
		$query->execute();
		return $con;
	};
	
?>
