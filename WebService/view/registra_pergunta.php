<?php

	require_once(dirname(__FILE__)."/../controller/profController.php");
	require_once(dirname(__FILE__)."/../model/perguntaModel.php");
	
	$id = $_GET["id"]; 
	$id_tema = $_GET["idtema"];
	$pergunta = $_GET["pergunta"]; 
	$resp1 = $_GET["resp1"];
	$resp2 = $_GET["resp2"];
	$resp3 = $_GET["resp3"];
	$resp4 = $_GET["resp4"];
	$respCorreta = $_GET["respCorreta"];
	//$op = $_GET["op"];
	
	$ctrl = new ProfController();
	$usr = new Pergunta($id);
	
	$usr->set_id_tema($id_tema);
	$usr->set_pergunta($pergunta);
	$usr->set_resp1($resp1);
	$usr->set_resp2($resp2);
	$usr->set_resp3($resp3);
	$usr->set_resp4($resp4);
	$usr->set_resp_correta($respCorreta);
	
	if($ctrl->cadastra_pergunta($usr)){/// retorna 1 se funcionar, retorna 0 se falhar
	$sucesso = array(
		'sucesso' => "1"
		);
	echo json_encode($sucesso);
	}
	else{
		$sucesso = array(
			'sucesso' => "0"
			);
	echo json_encode($sucesso);
	}
	
	?>