<?php

	require_once(dirname(__FILE__)."/../controller/profController.php");
	require_once(dirname(__FILE__)."/../model/profModel.php");
	$nome = $_POST["nome"];
	$id = $_POST["id"];  
	$senha = $_POST["senha"];
	$email = $_POST["email"];
	
	$ctrl = new ProfController();
	$usr = new Professor($id);
	
	$usr->set_nome($nome);
	$usr->set_senha($senha);
	$usr->set_email($email);
	
	if($ctrl->cadastra_prof($usr))
		echo "1";
	else
		echo "0";
	?>