<?php

class ProfController///apesar do nome da classe ser ProfController, ela acabou sendo o controller de tudo 
{
	private $con;
	
	public function __construct()
	{
		require_once(dirname(__FILE__)."/../conectarPiGame.php");
		$this->con = Conectar();
			
	}
	
	public function cadastra_prof($usr)
	{
		$id_prof = $usr->get_id_prof();
		$nome = $usr->get_nome();
		$senha = $usr->get_senha();
		$email = $usr->get_email();
		
		$query = $this->con->prepare("INSERT INTO professor VALUES (?,?,?,?)");
		$query->bindParam(1, $id_prof);
		$query->bindParam(2, $nome);
		$query->bindParam(3, $senha);
		$query->bindParam(4, $email);
		if(!$query->execute())
		{
			/*$query = $this->con->prepare("UPDATE usuario SET nome=?, senha=? WHERE id_usuario=?");
			$query->bindParam(1, $nome);
			$query->bindParam(2, $senha);
			$query->bindParam(3, $id_usuario);
			if(!$query->execute())
				return false;
			return true;*/
			return false;   //funcao de update acima, caso for utilizar, retirar este "return false";
		}
		return true;
		
	}
	
	public function cadastra_tema($usr)
	{
		$id_tema = $usr->get_id_tema();
		$tema = $usr->get_tema();
		
		$query = $this->con->prepare("INSERT INTO tema VALUES (?,?)");
		$query->bindParam(1, $id_tema);
		$query->bindParam(2, $tema);
		
		if(!$query->execute())
		{
			return false;   
		}
		return true;
		
	}
	
	public function cadastra_pergunta($usr)
	{
		$id_pergunta = $usr->get_id_pergunta();
		$id_tema = $usr->get_id_tema();
		$pergunta = $usr->get_pergunta();
		$resp1 = $usr->get_resp1();
		$resp2 = $usr->get_resp2();
		$resp3 = $usr->get_resp3();
		$resp4 = $usr->get_resp4();
		$resp_correta = $usr->get_resp_correta();
		
		$query = $this->con->prepare("INSERT INTO pergunta VALUES (?,?,?,?,?,?,?,?)");
		$query->bindParam(1, $id_pergunta);
		$query->bindParam(2, $id_tema);
		$query->bindParam(3, $pergunta);
		$query->bindParam(4, $resp1);
		$query->bindParam(5, $resp2);
		$query->bindParam(6, $resp3);
		$query->bindParam(7, $resp4);
		$query->bindParam(8, $resp_correta);
		
		
		if(!$query->execute())
		{
			return false;   
		}
		return true;
		
	}
	
	
	
	
	public function carrega_prof_login($email)
	{
		require_once(dirname(__FILE__)."/../model/profModel.php");
		$prof = new Professor(0);
		
		$query = $this->con->prepare("SELECT nome, senha, email FROM professor WHERE email=?");
		$query->bindParam(1, $email);
		$query->execute();
		
		$u = $query->fetch(PDO::FETCH_OBJ);
		
		if(!$u==null){
		$prof->set_nome($u->nome);
		$prof->set_senha($u->senha);
		$prof->set_email($u->email);
		}
		else{// se a consulta falhar
		$prof->set_nome("");
		$prof->set_senha("");
		$prof->set_email("");
		}
		return $prof;
		
	}
	
	public function lista_tema()//prepara uma objeto JSON com uma arrayJSON que tem varios objetos JSON inseridos nesta array para ser retornado
	{
		$lista = array();
		
		$query = $this->con->prepare
		("SELECT * FROM tema ORDER BY id");
		$query->execute();
		while($l = $query->fetch(PDO::FETCH_OBJ)){
			$temaSelec = array(
				'id'=>$l->id,
				'tema'=>$l->tema
			);
			array_push($lista, $temaSelec);
		}
		$lista2 = array(
		'dados'=>$lista
		);	
			
		return $lista2;
	}
	///
	/*
	
	
	public function remove_usuario($usr)
	{
		$id = $usr->get_id_usuario();
		$query = $this->con->prepare("DELETE FROM usuario WHERE id_usuario=?");
		$query->bindParam(1, $id);
		return ($query->execute());
	}
	*/
	/*while($l = $query->fetch(PDO::FETCH_OBJ)){
			$temaSelec = array(
				'id'=>$l->id,
				'tema'=>$l->tema
			);
			array_push($lista, $temaSelec);
		}*/
	
};


?>