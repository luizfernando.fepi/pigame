<?php

class Pergunta
{
	private $id_pergunta;
	private $id_tema;
	private $pergunta;
	private $resp1;
	private $resp2;
	private $resp3;
	private $resp4;
	private $resp_correta;
	
	public function __construct($id_pergunta)
	{
			$this->id_pergunta = $id_pergunta;
	}
	
	public function get_id_pergunta()
	{
		return $this->id_pergunta;
	}
	
	public function get_id_tema()
	{
		return $this->id_tema;
	}
	
	public function set_id_tema($valor)
	{
		$this->id_tema = $valor;
	}
	
	public function get_pergunta()
	{
		return $this->pergunta;
	}
	
	public function set_pergunta($valor)
	{
		$this->pergunta = $valor;
	}
	
	public function get_resp1()
	{
		return $this->resp1;
	}
	
	public function set_resp1($valor)
	{
		$this->resp1 = $valor;
	}
	
	public function get_resp2()
	{
		return $this->resp2;
	}
	
	public function set_resp2($valor)
	{
		$this->resp2 = $valor;
	}
	
	public function get_resp3()
	{
		return $this->resp3;
	}
	
	public function set_resp3($valor)
	{
		$this->resp3 = $valor;
	}
	public function get_resp4()
	{
		return $this->resp4;
	}
	
	public function set_resp4($valor)
	{
		$this->resp4 = $valor;
	}
	
	public function get_resp_correta()
	{
		return $this->resp_correta;
	}
	
	public function set_resp_correta($valor)
	{
		$this->resp_correta = $valor;
	}
	
};


?>