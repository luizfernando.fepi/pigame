<?php

class Tema
{
	private $id_tema;
	private $tema;
	

	
	public function __construct($id_tema)
	{
			$this->id_tema = $id_tema;
	}
	
	public function get_id_tema()
	{
		return $this->id_tema;
	}
	
	public function get_tema()
	{
		return $this->tema;
	}
	
	
	
	public function set_tema($valor)
	{
		$this->tema = $valor;
	}
	
	
};

?>