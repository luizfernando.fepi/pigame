<?php

class Professor
{
	private $id_prof;
	private $nome;
	private $senha;
	private $email;
	
	public function __construct($id_prof)
	{
			$this->id_prof = $id_prof;
	}
	
	public function get_id_prof()
	{
		return $this->id_prof;
	}
	
	public function get_nome()
	{
		return $this->nome;
	}
	
	public function get_senha()
	{
		return $this->senha;
	}
	
	public function get_email()
	{
		return $this->email;
	}
	
	public function set_nome($valor)
	{
		$this->nome = $valor;
	}
	
	public function set_senha($valor)
	{
		$this->senha = $valor;
	}
	
	public function set_email($valor)
	{
		$this->email = $valor;
	}
	
	
	
};

?>