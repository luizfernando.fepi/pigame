-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 09-Nov-2019 às 20:36
-- Versão do servidor: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pigame`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `Casa`
--

CREATE TABLE `Casa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_casa` enum('COMUM','DESAFIO','PREMIO_PUNICAO','') NOT NULL,
  `posicao_tabuleiro` int(20) UNSIGNED NOT NULL,
  `id_tabuleiro` int(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Disciplina`
--

CREATE TABLE `Disciplina` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Equipe`
--

CREATE TABLE `Equipe` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(30) NOT NULL,
  `codigo_acesso` varchar(10) NOT NULL,
  `posicao_tabuleiro` tinyint(3) UNSIGNED NOT NULL,
  `qtd_desafio` tinyint(3) UNSIGNED NOT NULL,
  `qtd_rotacao` tinyint(3) UNSIGNED NOT NULL,
  `cor` varchar(15) NOT NULL,
  `id_partida` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Partida`
--

CREATE TABLE `Partida` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo_acesso` varchar(10) NOT NULL,
  `id_tabuleiro` int(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Professor`
--

CREATE TABLE `Professor` (
  `id` int(20) UNSIGNED NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Questao`
--

CREATE TABLE `Questao` (
  `id` int(20) UNSIGNED NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  `alternativa_a` varchar(500) NOT NULL,
  `alternativa_b` varchar(500) NOT NULL,
  `alternativa_c` varchar(500) DEFAULT NULL,
  `alternativa_d` varchar(500) DEFAULT NULL,
  `alternativa_e` varchar(500) DEFAULT NULL,
  `resposta` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Tabuleiro`
--

CREATE TABLE `Tabuleiro` (
  `id` int(20) UNSIGNED NOT NULL,
  `qtdCasas` int(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Tema`
--

CREATE TABLE `Tema` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TemaDisciplina`
--

CREATE TABLE `TemaDisciplina` (
  `id_tema` int(10) UNSIGNED NOT NULL,
  `id_disciplina` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TemaPartida`
--

CREATE TABLE `TemaPartida` (
  `id_tema` int(10) UNSIGNED NOT NULL,
  `id_partida` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `TemaQuestao`
--

CREATE TABLE `TemaQuestao` (
  `id_tema` int(10) UNSIGNED NOT NULL,
  `id_questao` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Casa`
--
ALTER TABLE `Casa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tabuleiro` (`id_tabuleiro`);

--
-- Indexes for table `Disciplina`
--
ALTER TABLE `Disciplina`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `Equipe`
--
ALTER TABLE `Equipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_partida_equipe` (`id_partida`);

--
-- Indexes for table `Partida`
--
ALTER TABLE `Partida`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo_acesso` (`codigo_acesso`),
  ADD KEY `fk_tabuleiro_partida` (`id_tabuleiro`);

--
-- Indexes for table `Professor`
--
ALTER TABLE `Professor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `Questao`
--
ALTER TABLE `Questao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tabuleiro`
--
ALTER TABLE `Tabuleiro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tema`
--
ALTER TABLE `Tema`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `TemaDisciplina`
--
ALTER TABLE `TemaDisciplina`
  ADD PRIMARY KEY (`id_tema`,`id_disciplina`),
  ADD KEY `fk_disciplina` (`id_disciplina`);

--
-- Indexes for table `TemaPartida`
--
ALTER TABLE `TemaPartida`
  ADD PRIMARY KEY (`id_tema`,`id_partida`),
  ADD KEY `fk_partida_tema` (`id_partida`);

--
-- Indexes for table `TemaQuestao`
--
ALTER TABLE `TemaQuestao`
  ADD PRIMARY KEY (`id_tema`,`id_questao`),
  ADD KEY `fk_questao_tema` (`id_questao`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Professor`
--
ALTER TABLE `Professor`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Tabuleiro`
--
ALTER TABLE `Tabuleiro`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `Casa`
--
ALTER TABLE `Casa`
  ADD CONSTRAINT `fk_tabuleiro` FOREIGN KEY (`id_tabuleiro`) REFERENCES `Tabuleiro` (`id`);

--
-- Limitadores para a tabela `Equipe`
--
ALTER TABLE `Equipe`
  ADD CONSTRAINT `fk_partida_equipe` FOREIGN KEY (`id_partida`) REFERENCES `Equipe` (`id`);

--
-- Limitadores para a tabela `Partida`
--
ALTER TABLE `Partida`
  ADD CONSTRAINT `fk_tabuleiro_partida` FOREIGN KEY (`id_tabuleiro`) REFERENCES `Tabuleiro` (`id`);

--
-- Limitadores para a tabela `TemaDisciplina`
--
ALTER TABLE `TemaDisciplina`
  ADD CONSTRAINT `fk_disciplina` FOREIGN KEY (`id_disciplina`) REFERENCES `Disciplina` (`id`),
  ADD CONSTRAINT `fk_tema` FOREIGN KEY (`id_tema`) REFERENCES `Tema` (`id`);

--
-- Limitadores para a tabela `TemaPartida`
--
ALTER TABLE `TemaPartida`
  ADD CONSTRAINT `fk_partida_tema` FOREIGN KEY (`id_partida`) REFERENCES `Partida` (`id`),
  ADD CONSTRAINT `fk_tema_partida` FOREIGN KEY (`id_tema`) REFERENCES `Tema` (`id`);

--
-- Limitadores para a tabela `TemaQuestao`
--
ALTER TABLE `TemaQuestao`
  ADD CONSTRAINT `fk_questao_tema` FOREIGN KEY (`id_questao`) REFERENCES `Questao` (`id`),
  ADD CONSTRAINT `fk_tema_questao` FOREIGN KEY (`id_tema`) REFERENCES `Tema` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
